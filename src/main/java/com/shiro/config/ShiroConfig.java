package com.shiro.config;



import com.shiro.service.ShiroRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.LinkedHashMap;
import java.util.Map;

@SpringBootConfiguration
public class ShiroConfig {
    //3.创建配置ShiroFilterFactoryBean
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        //设置安全管理器
        bean.setSecurityManager(securityManager);
        //拦截--设置shiro内置过滤器
        Map<String ,String> map =new LinkedHashMap<>();
        map.put("/user/*", "pr");
        map.put("/user/**", "anon");
        map.put("/**", "authc");

        bean.setFilterChainDefinitionMap(map);
        //设置登录页面
        bean.setLoginUrl("/Login");
        return bean;
    }

    //2.创建DefaultWebSecurityManager
    @Bean
    public DefaultWebSecurityManager securityManager(Realm realm){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager(realm);
        return securityManager;
    }

    //1.创建realm对象
    @Bean
    public Realm realm(){

        return new ShiroRealm();
    }
}
