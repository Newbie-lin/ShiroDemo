package com.shiro.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonResult {
    private Object data;
    private String mcg;
    private Integer status;

    public static JsonResult ok(){
        return new JsonResult(null,"登录成功",200);
    }
    public static JsonResult ok(Object data){
        return new JsonResult(data,"登录成功",200);
    }
    public static JsonResult no(){
        return new JsonResult(null,"登录失败",201);
    }

}
