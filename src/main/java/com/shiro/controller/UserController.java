package com.shiro.controller;

import com.shiro.pojo.User;
import com.shiro.service.UserService;
import com.shiro.vo.JsonResult;
import org.apache.catalina.security.SecurityUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.security.util.Password;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/{username}/{password}")
    public JsonResult getUser(User user){
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword());

        subject.login(token);
        return JsonResult.ok();
    }
}
