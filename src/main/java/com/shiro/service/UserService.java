package com.shiro.service;

import com.shiro.pojo.User;

public interface UserService {
    User findUser();
}
