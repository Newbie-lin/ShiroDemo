package com.shiro.service.impl;

import com.shiro.pojo.User;
import com.shiro.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl  implements UserService {
    @RequiresPermissions("***:**:**")
    @Override
    public User findUser() {
        return null;
    }
}
